import math, pylab

class ifs:
    def __init__(self, initState = {}, wts = {}, ufunc = None):
        self.state = initState
        self.wts = wts
        self.ufunc = ufunc

    def setState(self, state):
        self.state = state

    def update(self):
        output, nextState = {}, {}
        for variable in self.state:
            output[variable] = self.ufunc(self.state[variable])
            nextState[variable] = 0

#Robert Daland set some kind of record here for the most arcane way to achieve a matrix multiply.

        for wt in self.wts:
            nextState[wt[0]] += self.wts[wt]*output[wt[1]]

        for variable in self.state:
            self.state[variable] = nextState[variable]


    
# Use default values for a highly categorical decision rule with threshold in the middle. 

    
def logistic(x, kappa = 500, sigma = -1000): 
	etothe = math.exp(kappa + (sigma * x))
	return(1.0/(1 + etothe))
	
def identical(x):
	return(x)
	

def vecFlowPlot(ifs, xRange = [.05*i for i in xrange(21)], yRange = [.05*i for i in xrange(21)]):
    X, Y, U, V = [], [], [], []

    for x in xRange:
        for y in yRange:
            X.append(x)
            Y.append(y)
            ifs.setState({0: x, 1: y})
            ifs.update()
            U.append(ifs.state[0]-x)
            V.append(ifs.state[1]-y)

    pylab.quiver(X, Y, U, V, pivot = 'middle')



#ufunc has been set up this way so you can change the input-output rule without modifying the rest of the code.

ufunc = lambda x: logistic(x, kappa, sigma)
# ufunc = lambda x: identical(x)

# The .25 values here are not doing any work. They are placeholders to set up the data structure

init = {0: .25, 1: .25}

alpha = .1

#The is the matrix for the learning rule represented as a dictionary

wts = {(0,0): 1-alpha, (0,1): alpha, (1,0): alpha, (1,1): 1-alpha}
phi = ifs(init, wts, ufunc)

# vecFlowPlot(phi)



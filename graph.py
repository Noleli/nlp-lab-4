import random

class graph():
    def __init__(self, links = {}, nodes = {}):
        self.links = links or {}
        self.nodes = nodes or {}
        self.nNodes = len(self.nodes)
        
        if not self.nodes:
            self.getNodesFromLinks()

    def getNodesFromLinks(self):
        for link in self.links:
            self.nodes[link[0]], self.nodes[link[1]] = 1, 1
        self.nNodes = len(self.nodes)

    def inNbs(self, node):
        inNbs = {}
        for link in self.links:
            if link[1] == node: inNbs[link[0]] = 1
        return(inNbs.keys())

    def outNbs(self, node):
        outNbs = {}
        for link in self.links:
            if link[0] == node: outNbs[link[1]] = 1
        return(outNbs.keys())

    def randLink(self, lNodes, rNodes, p, linkWt = 1):
        for lNode in lNodes:
            for rNode in rNodes:
                if random.random() < p:
                    self.links[(lNode, rNode)] = linkWt
                    self.links[(rNode, lNode)] = linkWt
                    
#The following generates a scale-free graph, using the Barabasi-Albert algorithm
#nParents controls the average degree. nParents = 2 gives average degree 4.

def BarabasiAlbert(nNodes, nParents, linkWt = 1):
    links = {}

    ## STEP 1: fully connected seed
    seedSize = max(nParents, 2)
    for iNode in xrange(seedSize):
        for jNode in xrange(seedSize):
            if iNode != jNode:
                links[(iNode, jNode)], links[(iNode, jNode)] = linkWt, linkWt
    g = graph(links)

    ## STEP 2: add one node at a time until done
    while g.nNodes < nNodes:
        ## select parents
        newNode = g.nNodes
        parents = {}
        while len(parents) < nParents:
            parents[g.links.keys()[random.randint(0,len(g.links)-1)][0]] = 1

        ## add new node and link it to/from parents
        g.nodes[newNode] = 1
        g.nNodes += 1
        for parent in parents:
            g.links[(parent, newNode)], g.links[(newNode, parent)] = 1, 1
            
    return(g)

#The following merges two graphs together. You don't need this for the lab. I'm leaving
#here in case you want the code for your project.

def merge(left, right):
    'Merge with another graph'
    'Relabel other graph to start at self.nNodes'
    'Return index of partition between old and new nodes'

    new = graph(left.links, left.nodes)
    rNodeLabels = right.nodes.keys()
    nodeOffset = left.nNodes
    nodeMap = {}
    for iNode in xrange(len(rNodeLabels)):
        nodeMap[rNodeLabels[iNode]] = iNode + nodeOffset

    for node in rNodeLabels:
        new.nodes[nodeMap[node]] = right.nodes[node]
    for link in right.links:
        new.links[(nodeMap[link[0]], nodeMap[link[1]])] = right.links[link]

    ## bookkeeping: update nNodes to include new nodes
    new.nNodes += right.nNodes
    return(new)

#The following generates a graph consisting of two subgroups. Each subgroup has as
#scale-free structure with average degree kIntra. The members of the group have an average
#kInter links to members of the other group. wtInter defines the weights of the inter-group
#links.

def twoPops(n, k, kInter, wtInter = 1.0, wt = 1.0, n1 = 0, k1 = 0, wt1 = 0):
    if n1:  n2 = n1
    else:   n2 = n
    if k1:  k2 = k1
    else:   k2 = k
    if wt1: wt2 = wt1
    else:   wt2 = wt
    
    x = BarabasiAlbert(n, k/2, wt)
    y = BarabasiAlbert(n2, k2/2, wt2)
    z, partition = merge(x, y), n
    xNodes, yNodes = range(partition), range(partition, z.nNodes)
    pInter = .5 * float(kInter) * (len(xNodes) + len(yNodes))/(len(xNodes)*len(yNodes))
    z.randLink(xNodes, yNodes, pInter, wtInter)
    return(z)


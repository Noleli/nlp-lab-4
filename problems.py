execfile('ifs_5_12.py')

for kappa in xrange(0, 10):
# for sigma in [-1000,-100] + [i for i in xrange(-90,-10,10)] + [i for i in xrange(-10,0)]:
	xVals = [.02*i for i in xrange(51)]
	# kappa = 5
	sigma = -10
	print kappa
	yVals = [logistic(x, kappa, sigma) for x in xVals]
	pylab.plot(xVals, yVals, hold=True)
pylab.show()
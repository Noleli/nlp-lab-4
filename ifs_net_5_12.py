
import math, graph, random, pylab


def logistic(x, kappa = 5, sigma = -10): 
	etothe = math.exp(kappa + (sigma * x))
	return(1.0/(1 + etothe))

#slope and intercept there so you can put in different linear functions if you want.
def linear (x, slope = 1, intercept = 0):
	return(slope * x + intercept)

class agent:
    'Agent class. Keeps a dictionary of parameters.'
    'Generate parameters and initial state from gen distributions'
    def __init__(self, nodeIndex, genParams = None, genInitState = None):
        self.index = nodeIndex
        
        if genParams == None: self.params = genParams
        else: self.params = genParams(nodeIndex)

        if genInitState == None: self.state = 0.0
        else: self.state = genInitState(self)
        
        self.output = 0.0

def splitBernoulli(index, split, pMinus, pPlus):
    if index < split: return({'p': pMinus})
    else: return({'p': pPlus})

def Bernoulli(agent):
    if random.random() < agent.params['p']: return(1.0)
    else: return(0.0)


class ifs_net:
    def __init__(self, graph = None, genAgentParams = None, genInitState = None):
        self.graph = graph
        self.genAgentParams = genAgentParams
        self.genInitState = genInitState
        for node in self.graph.nodes.keys():
            self.graph.nodes[node] = agent(node, genAgentParams, genInitState)

    def resetParams(self, genAgentParams):
        for agent in self.graph.nodes:
            self.graph.nodes[agent].params = genAgentParams(agent)

    def resetInitState(self):
        for agent in self.graph.nodes:
            self.graph.nodes[agent].state = self.genInitState(self.graph.nodes[agent])

    def setOutputFunction(self, ofunc):
        'Determines the output for each agent'
        self.ofunc = ofunc

    def setUpdateRule(self, urule):
        'Determines the next state for each agent based on output of their neighbors'
        self.urule = urule
    
    def update(self):
        'Update cycle'

        ## step 1: calculate output using ofunc
        for iAgent in self.graph.nodes:
            curAgent = self.graph.nodes[iAgent]
            curAgent.output = self.ofunc(curAgent.state, curAgent.params)

        ## step 2: calculate next state using urule and update
        nextState = self.urule(self)
        for iAgent in nextState:
            self.graph.nodes[iAgent].state = nextState[iAgent]

    def meanState(self, agentSet):
        s = 0.0
        for agent in agentSet:
            s += self.graph.nodes[agent].state
        return(s / len(agentSet))

def stochasticSum(ifsnet, alpha):
    'Accepts an ifs as input'
    'Calculates next state for each agent by aggregating over current state/output'
    'Returns a dictionary containing next state for each agent'
    wtSum = dict([(iNode, 0.) for iNode in ifsnet.graph.nodes])
    wtdSum = dict([(iNode, 0.) for iNode in ifsnet.graph.nodes])

    links = ifsnet.graph.links
    for link in links:
        wt = links[link]
        wtSum[link[1]] += alpha*wt
        wtdSum[link[1]] += alpha*wt*ifsnet.graph.nodes[link[0]].output

    for node in wtdSum.keys():
        wtdSum[node] += (1.0 - wtSum[node]) * ifsnet.graph.nodes[node].output
        if wtdSum[node] < 0.0: wtdSum[node] = 0.0
        if wtdSum[node] > 1.0: wtdSum[node] = 1.0

    return(wtdSum)
    

## NOTES:
##          In order to allow the modeler maximum control over the dynamics,
##  the modeler is allowed (forced) to specify the output function and
##  update rule.
##          The output function determines the output state of an
##  agent; its behavior can be made to vary from agent to agent by
##  making use of agent-specific parameters. In general, it should depend
##  only on the agent's current state and parameters, a constraint which
##  is softly enforced by making only these values its arguments.
##          The update rule controls how the state of an agent is updated.
##  This is where social effects should be localized. Thus, the update rule
##  may need to make reference to the neighbors of the current node, and in
##  particular to their state and/or output.
##          The formal implementation requires urule to output a
##  dictionary whose keys are the node indices and whose values are the next
##  state for that node. This is for purposes of computational efficiency.
##  There are update rules which can be implemented in parallel (on all agents)
##  with a single pass through the link dictionary, which if implemented
##  serially (one agent at a time) would require a separate pass through the
##  link dictionary for each agent. If the modeler is employing such a rule,
##  this setup allows for a massive savings in operations. But if not, there is
##  no extra operation cost, all it means is the modeler has to wrap the next
##  states into a dictionary, which is a single extra line of code.

def reset(ifsnet, pStar, pPlain):
    BernoulliParam = lambda iNode: splitBernoulli(iNode, n, pStar, pPlain)
    ifsnet.resetParams(BernoulliParam)
    ifsnet.resetInitState()

def netFlowPlot(ifsnet, pStarRange = [.05*i for i in xrange(21)], pPlainRange = [.05*i for i in xrange(21)]):
    X, Y, U, V = [], [], [], []
    n = len(ifsnet.graph.nodes)/2
    for pStar in pStarRange:
        for pPlain in pPlainRange:
            X.append(pStar)
            Y.append(pPlain)
            reset(ifsnet, pStar, pPlain)
            for i in xrange(100):
                ifsnet.update()
            U.append(ifsnet.meanState(range(n)) - pStar)
            V.append(ifsnet.meanState(range(n, 2*n)) - pPlain)
    pylab.quiver(X, Y, U, V, pivot = 'middle')

##def linkDist(g):
##	n = len(g.nodes)/2
##	lSS, lSP, lPS, lPP = 0, 0, 0, 0
##	for link in g.links:
##		if link[0] < n and link[1] < n: lSS += 1
##		if link[0] < n and link[1] >= n: lSP += 1
##		if link[0] >= n and link[1] < n: lPS += 1
##		if link[0] >= n and link[1] >= n: lPP += 1
##	print lSS, lSP, lPS, lPP

n = 40
kIntra = 4
kInter = 2
#Use negative values of wtInter to get negative coupling
#wtInter = -1.0
#Use positive values of wtInter to get positive coupling
wtInter = 1.0
pStar = 0.75
pPlain = 0.25
alpha = 0.03
kappa = 5
sigma = -10

g = graph.twoPops(n, kIntra, kInter, wtInter)

#Here is the logistic used to model categorization
ofunc = lambda state, params: logistic(state, kappa, sigma)

#Here is the linear version hardwired to the identity function
#ofunc = lambda state, params: linear(state, 1, 0)

urule = lambda ifsnet: stochasticSum(ifsnet, alpha)
BernoulliParam = lambda iNode: splitBernoulli(iNode, n, pStar, pPlain)

m = ifs_net(g, BernoulliParam, Bernoulli)

m.setOutputFunction(ofunc)
m.setUpdateRule(urule)
netFlowPlot(m)

